//
//  MainScene.hpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/20
//
//

#ifndef __rakuran__MainScene__
#define __rakuran__MainScene__

#include "cocos2d.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "PartsFooter.hpp"

USING_NS_CC;
using namespace ui;

struct RankInfoDTO;

class PartsTableView;

class MainScene : public Scene
{
    PartsTableView* _ui;
  public:
    static MainScene* createScene();

    MainScene();
    virtual ~MainScene();

    virtual bool init() override;
    virtual void onEnter() override;
    virtual void onEnterTransitionDidFinish() override;
    virtual void onExit() override;
    virtual void onExitTransitionDidStart() override;
    virtual void callback(std::vector<RankInfoDTO> list);
    
  private:
    
    enum class ZOrder
    {
        BG,
        TABLE_VIEW,
        HEADER,
        FOOTER,
    };
    
    void initUI();

};


#endif /* defined(__rakuran__MainScene__) */
