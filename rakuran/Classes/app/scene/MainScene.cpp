//
//  MainScene.cpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/20
//
//

#include "MainScene.hpp"
#include "AppMacro.h"
#include "PartsFooter.hpp"
#include "PartsHeader.hpp"
#include "PartsTableView.hpp"
#include "PartsTableViewCell.hpp"
#include "RakutenInfoService.hpp"
#include "RankInfoDTO.h"

namespace
{
}  // namespace

MainScene* MainScene::createScene()
{
    TRACE;
    auto scene = new MainScene();
    scene->autorelease();
    scene->init();
    return scene;
}

MainScene::MainScene()
{
    // constructor
    TRACE;
}

MainScene::~MainScene()
{
    // destructor
    TRACE;
}

bool MainScene::init()
{
    TRACE;
    auto ret = Scene::init();
    if (ret)
    {
        initUI();
    }
    return true;
}

void MainScene::onEnter()
{
    TRACE;
    Scene::onEnter();
}

void MainScene::onEnterTransitionDidFinish()
{
    TRACE;
    Scene::onEnterTransitionDidFinish();
}

void MainScene::onExit()
{
    TRACE;
    Scene::onExit();
}

void MainScene::onExitTransitionDidStart()
{
    TRACE;
    Scene::onExitTransitionDidStart();
}

void MainScene::callback(std::vector<RankInfoDTO> list)
{
  _ui->setRankInfoList(list);
}

void MainScene::initUI()
{
    TRACE;
    auto panel = Layout::create();
    panel->setContentSize(MAIN_VIEW_SIZE);
    panel->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
    panel->setBackGroundColor(Color3B::WHITE);
    this->addChild(panel, static_cast<int>(ZOrder::BG));

    auto nodeHeader = PartsHeader::create();
    nodeHeader->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    nodeHeader->setPosition(Vec2(0, MAIN_VIEW_HEIGHT));
    this->addChild(nodeHeader, static_cast<int>(ZOrder::HEADER));

    auto nodeFooter = PartsFooter::create();
    nodeFooter->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    nodeFooter->setPosition(Vec2(0, 0));
    this->addChild(nodeFooter, static_cast<int>(ZOrder::FOOTER));

    nodeFooter->setOnTouchButton([](PartsFooter::ButtonType type) {
        switch (type)
        {
            case PartsFooter::ButtonType::LIST:
                MessageBox("おされたよ", "Listボタン");
                break;
            case PartsFooter::ButtonType::MODULE:
                MessageBox("おされたよ", "Moduleボタン");
                break;
            case PartsFooter::ButtonType::ARRAY:
                MessageBox("おされたよ", "Arrayボタン");
                break;
            case PartsFooter::ButtonType::SETTING:
                MessageBox("おされたよ", "Settingボタン");
                break;
            default:
                break;
        }
    });

    auto headerHeight = nodeHeader->getContentSize().height;
    auto footerHeight = nodeFooter->getContentSize().height;
    auto height       = MAIN_VIEW_SIZE.height - (headerHeight + footerHeight);

     _ui = PartsTableView::create(Size(MAIN_VIEW_SIZE.width, height));
    _ui->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    _ui->setPosition(Vec2(0, footerHeight));
    _ui->setOnSelected(
        [=](const int rank) { MessageBox(("rank[" + std::to_string(rank) + "]").c_str(), "onSelected"); });
    this->addChild(_ui, static_cast<int>(ZOrder::TABLE_VIEW));
    RakutenInfoService *rakutenInfo = RakutenInfoService::getInstance();
   rakutenInfo->RakutenInfoService::getRankInfoList(CC_CALLBACK_1(MainScene::callback, this));

}
