//
//  PartsHeader.hpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/20
//
//

#ifndef __rakuran__PartsHeader__
#define __rakuran__PartsHeader__

#include "cocos2d.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

class PartsHeader : public Node
{
  public:
    CREATE_FUNC(PartsHeader);

  private:
    PartsHeader();
    virtual ~PartsHeader();
    virtual bool init() override;
    virtual void onEnter() override;
    virtual void onEnterTransitionDidFinish() override;
    virtual void onExit() override;
    virtual void onExitTransitionDidStart() override;

    virtual void initUI();

  private:
    Node* _csb;
};

#endif /* defined(__rakuran__PartsHeader__) */
