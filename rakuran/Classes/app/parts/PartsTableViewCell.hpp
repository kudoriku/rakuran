//
//  PartsTableViewCell.hpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/20
//
//

#ifndef __rakuran__PartsTableViewCell__
#define __rakuran__PartsTableViewCell__

#include "cocos2d.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

class PartsTableViewCell : public Node
{
  public:
    CREATE_FUNC(PartsTableViewCell);

    void initWithViewCellInfo(const int rank, const std::string& name);

  private:
    PartsTableViewCell();
    virtual ~PartsTableViewCell();
    virtual bool init() override;
    virtual void onEnter() override;
    virtual void onEnterTransitionDidFinish() override;
    virtual void onExit() override;
    virtual void onExitTransitionDidStart() override;
    
    virtual void initUI();
    
private:
        Node* _csb;
};

#endif /* defined(__rakuran__PartsTableViewCell__) */
