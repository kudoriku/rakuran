//
//  PartsFooter.cpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/21.
//
//

#include "PartsFooter.hpp"
#include "AppMacro.h"
#include "const.h"

namespace
{
const char* CSB_NAME = "footer.csb";
}  // namespace

PartsFooter::~PartsFooter()
{
    // destructor
    TRACE;
}

PartsFooter::PartsFooter()
    : _onTouchButton(nullptr)
{
        // constructor
        TRACE;
    }



bool PartsFooter::init()
{
    TRACE;
    auto ret = Node::init();
    if (ret)
    {
        initUI();
    }
    return ret;
}

void PartsFooter::onEnter()
{
    TRACE;
    Node::onEnter();
}

void PartsFooter::onEnterTransitionDidFinish()
{
    TRACE;
    Node::onEnterTransitionDidFinish();
}

void PartsFooter::onExit()
{
    TRACE;
    Node::onExit();

    this->unscheduleUpdate();
}

void PartsFooter::onExitTransitionDidStart()
{
    TRACE;
    Node::onExitTransitionDidStart();
}

void PartsFooter::onButtonTouched(Ref* ref, Widget::TouchEventType eventType, ButtonType type)
{
        if (eventType != Widget::TouchEventType::ENDED)
        {
            return;
        }
        
        if (_onTouchButton != nullptr)
        {
            _onTouchButton(type);
        }

}


void PartsFooter::initUI()
{
    // csbファイルのロード処理
    _csb = CSLoader::getInstance()->createNodeWithFlatBuffersFile(CSB_NAME);
    this->setContentSize(_csb->getContentSize());
    this->addChild(_csb);

    auto listButton = _csb->getChildByName<Button*>("btn_list");
    auto moduleButton = _csb->getChildByName<Button*>("btn_module");
 auto arrayButton = _csb->getChildByName<Button*>("btn_array");
    auto settingButton = _csb->getChildByName<Button*>("btn_setting");

listButton->addTouchEventListener(CC_CALLBACK_2(PartsFooter::onButtonTouched, this, ButtonType::LIST));
    moduleButton->addTouchEventListener(CC_CALLBACK_2(PartsFooter::onButtonTouched, this, ButtonType::MODULE));
    arrayButton->addTouchEventListener(CC_CALLBACK_2(PartsFooter::onButtonTouched, this, ButtonType::ARRAY));
    settingButton->addTouchEventListener(CC_CALLBACK_2(PartsFooter::onButtonTouched, this, ButtonType::SETTING));

    
}

void PartsFooter::setOnTouchButton(OnTouchButton cb)
{
    _onTouchButton = cb;
}


