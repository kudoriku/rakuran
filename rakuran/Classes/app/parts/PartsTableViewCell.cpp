//
//  PartsTableViewCell.cpp
//  rakuran
//
//  Created by 工藤陸 on 03/09/18
//
//

#include "PartsTableViewCell.hpp"
#include "AppMacro.h"
#include "const.h"

namespace
{
    const char* CSB_NAME = "list_item_s.csb";
}  // namespace

PartsTableViewCell::PartsTableViewCell()
{
    // constructor
    TRACE;
}

PartsTableViewCell::~PartsTableViewCell()
{
    // destructor
    TRACE;
}

bool PartsTableViewCell::init()
{
    TRACE;
    auto ret = Node::init();
    if (ret)
    {
        initUI();
    }
    return ret;
}

void PartsTableViewCell::onEnter()
{
    TRACE;
    Node::onEnter();
}

void PartsTableViewCell::onEnterTransitionDidFinish()
{
    TRACE;
    Node::onEnterTransitionDidFinish();
}

void PartsTableViewCell::onExit()
{
    TRACE;
    Node::onExit();

    this->unscheduleUpdate();
}

void PartsTableViewCell::onExitTransitionDidStart()
{
    TRACE;
    Node::onExitTransitionDidStart();
}

void PartsTableViewCell::initUI()
{
    // csbファイルのロード処理
    _csb = CSLoader::getInstance()->createNodeWithFlatBuffersFile(CSB_NAME);
    this->setContentSize(_csb->getContentSize());
    this->addChild(_csb);
    
    auto panel = dynamic_cast<Layout*>(_csb->getChildByName("panel"));
    panel->setTouchEnabled(true);
    panel->setSwallowTouches(false);
}

void PartsTableViewCell::initWithViewCellInfo(const int rank, const std::string& name)
{
    auto panel = _csb->getChildByName("panel");
    auto rankNode = dynamic_cast<Text*>(panel->getChildByName("rank"));
    rankNode->setString(std::to_string(rank));
    auto titleNode =dynamic_cast<Text*>(panel->getChildByName("title"));
    titleNode->setString(name);
}
