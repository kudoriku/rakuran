//
//  PartsFooter.hpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/20
//
//

#ifndef __rakuran__PartsFooter__
#define __rakuran__PartsFooter__

#include "cocos2d.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

class PartsFooter : public Node
{
  public:
    CREATE_FUNC(PartsFooter);

    enum class ButtonType
    {
        LIST,
        MODULE,
        ARRAY,
        SETTING,
    };

    using OnTouchButton = std::function<void(const ButtonType type)>;
    void setOnTouchButton(OnTouchButton cb);

  private:
    PartsFooter();
    virtual ~PartsFooter();
    virtual bool init() override;
    virtual void onEnter() override;
    virtual void onEnterTransitionDidFinish() override;
    virtual void onExit() override;
    virtual void onExitTransitionDidStart() override;
    virtual void onButtonTouched(Ref* ref, Widget::TouchEventType eventType, ButtonType type);

    virtual void initUI();

  private:
    Node*         _csb;
    OnTouchButton _onTouchButton;
};

#endif /* defined(__rakuran__PartsFooter__) */
