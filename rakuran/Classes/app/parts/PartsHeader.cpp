//
//  PartsHeader.cpp
//  rakuran
//
//  Created by 工藤陸 on 03/06/21.
//
//

#include "PartsHeader.hpp"
#include "AppMacro.h"
#include "const.h"

namespace
{
const char* CSB_NAME = "header.csb";
}  // namespace

PartsHeader::PartsHeader()
{
    // constructor
    TRACE;
}

PartsHeader::~PartsHeader()
{
    // destructor
    TRACE;
}

bool PartsHeader::init()
{
    TRACE;
    auto ret = Node::init();
    if (ret)
    {
        initUI();
    }
    return ret;
}

void PartsHeader::onEnter()
{
    TRACE;
    Node::onEnter();
}

void PartsHeader::onEnterTransitionDidFinish()
{
    TRACE;
    Node::onEnterTransitionDidFinish();
}

void PartsHeader::onExit()
{
    TRACE;
    Node::onExit();

    this->unscheduleUpdate();
}

void PartsHeader::onExitTransitionDidStart()
{
    TRACE;
    Node::onExitTransitionDidStart();
}

void PartsHeader::initUI()
{
    // csbファイルのロード処理
    _csb = CSLoader::getInstance()->createNodeWithFlatBuffersFile(CSB_NAME);
    this->setContentSize(_csb->getContentSize());
    this->addChild(_csb);
}
