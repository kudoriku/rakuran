//
//  RakutenInfoService.hpp
//  rakuran
//
//  Created by 工藤陸 on 03/05/14.
//
//

#ifndef __rakuran__RakutenInfoService__
#define __rakuran__RakutenInfoService__

#include "cocos2d.h"

using namespace cocos2d;

struct RankInfoDTO;

class RakutenInfoService
{
  public:
    static RakutenInfoService* getInstance();
    static void                destory();
    
    using callback = std::function<void(std::vector<RankInfoDTO>)>;
    void getRankInfoList(callback);
    
    std::string getRakutenID();
    
  private:
    static RakutenInfoService* _instance;
    RakutenInfoService();
    virtual ~RakutenInfoService(){};
    
    callback _callback;
};

#endif /* defined(__rakuran__RakutenInfoService__) */
