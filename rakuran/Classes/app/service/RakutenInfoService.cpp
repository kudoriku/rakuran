//
//  RakutenInfoService.cpp
//  rakuran
//
//  Created by 工藤陸 on 03/05/14.
//
//

#include "RakutenInfoService.hpp"
#include "AppMacro.h"
#include "RankInfoDTO.h"
#include "network/HttpClient.h"

USING_NS_CC;
using namespace cocos2d::network;

namespace
{
    const int kIdSize = 64;
    const char* kRequestURL = "https://app.rakuten.co.jp/services/api/IchibaItem/Ranking/20170628?format=json&applicationId=";
}

RakutenInfoService* RakutenInfoService::_instance = nullptr;

RakutenInfoService::RakutenInfoService()
: _callback(nullptr)
{
}

RakutenInfoService* RakutenInfoService::getInstance()
{
    if (NULL == _instance)
    {
        _instance = new RakutenInfoService();
    }
    return _instance;
}

void RakutenInfoService::destory()
{
    delete _instance;
    _instance = nullptr;
}

void RakutenInfoService::getRankInfoList(callback cb)
{
    _callback = cb;
    
    auto request = new HttpRequest();
    
    auto id = getRakutenID();
    std::string url = kRequestURL + id;
    request->setUrl(url.c_str());
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback([this](HttpClient* client, HttpResponse* response)
    {
        const char* json = &(*response->getResponseData())[0];
        LOG("%s",json);
        
        Director::getInstance()->getScheduler()->performFunctionInCocosThread([ = ] ()
        {
            if (_callback == nullptr) {
                return;
            }
            
            std::vector<RankInfoDTO> list;

            for (int i = 1; i <= 20; ++i)
            {
                RankInfoDTO dto;
                dto.rank       = i;
                dto.name       = cocos2d::StringUtils::format("商品名%d", i);
                dto.imageURL_S = "https://thumbnail.image.rakuten.co.jp/@0_mall/kyunan/cabinet/zakkokuspecial/sannjyukokumai/"
                "imgrc0072298427.jpg?_ex=64x64";
                dto.imageURL_L = "https://thumbnail.image.rakuten.co.jp/@0_mall/kyunan/cabinet/zakkokuspecial/sannjyukokumai/"
                "tamachan-zakkoku.jpg?_ex=128x128";
                list.push_back(dto);
            }
            
            _callback(list);
        });
        
    });
    
    auto client = HttpClient::getInstance();
    client->enableCookies(NULL);
    client->send(request);
}

std::string RakutenInfoService::getRakutenID()
{
    std::string path = FileUtils::getInstance()->fullPathForFilename("id.txt");

    FILE *fp;
    char str[kIdSize];
    
    fp = fopen(path.c_str(), "r");   // ファイルを開く。失敗するとNULLを返す。
    if(fp != NULL)
    {
        fgets(str, kIdSize, fp);
    }
    fclose(fp);
    return &str[0];
}
