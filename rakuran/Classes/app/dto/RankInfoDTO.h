//
//  RankInfoDTO.h
//  rakuran
//
//  Created by kudo on 03/01/18.
//
//

#ifndef __rakuran__RankInfoDTO__
#define __rakuran__RankInfoDTO__

struct RankInfoDTO
{
    /**
     *  @brief ランキングの順位
     */
    int rank;

    /**
     *  @brief 商品名
     */
    std::string name;

    /**
     *  @brief 商品画像のURLスモールサイズ
     */
    std::string imageURL_S;

    /**
     *  @brief 商品画像のURLビッグサイズ
     */
    std::string imageURL_L;
};

#endif /* defined(__rakuran__RankInfoDTO__) */
